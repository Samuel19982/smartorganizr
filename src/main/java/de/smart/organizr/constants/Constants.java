package de.smart.organizr.constants;

/**
 * Constants which are used by more than one class
 */
public class Constants {

	private Constants(){
	}

	public static String getCSSCorrectClass(){
		return "evaluation-icon-correct";
	}

	public static String getCSSWrongClass(){
		return "evaluation-icon-wrong";
	}
}
